from flask import Flask, request,jsonify,session,redirect,url_for,flash
from flask import render_template
from flask_pymongo import PyMongo
import os
import json
import uuid
import hashlib
from bson.json_util import dumps
app = Flask(__name__)
app.config['MONGO_URI']="mongodb://127.0.0.1:27017/voting"
mongo= PyMongo(app)
app.secret_key = 'super secret key'
app.config['SESSION_TYPE'] = 'filesystem'

@app.route('/' ,methods=['POST','GET'])

def login():
 if "user" in session:
  session.pop("user",None)
  flash("You have voted successfully!")
  return redirect(url_for('.index'))
 else: 
  if request.method == "POST":
   user =request.form['user']
   pwd =request.form['pwd']
   pwd= hashlib.md5(request.form['pwd'].encode())
   pwd=pwd.hexdigest()
   
   password =mongo.db.Users.find_one({"user":user},{"pwd":1})
   voted=mongo.db.Users.find_one({"user":user},{"voted":1})
   print(voted)
   if password == None or user == None:
     flash("Please enter both the feilds")
     return render_template("login.html")
   elif password["pwd"]!=pwd:
     flash("Invalid Username/Password")
     return render_template("login.html")    
   elif voted["voted"]==True:
     flash("Sorry it seems like you have already voted")
     return render_template("login.html") 
   elif password != None and password["pwd"]==pwd:
     session['user']= user
     session['online']= 1
     return redirect(url_for("user"))
   else:
     flash("Invalid username/password")
     return render_template("login.html")
  else: 
   return render_template("login.html")


@app.route('/cp')

def index():
    rows=mongo.db.Cantidates.find().count()
    list =[]
    for x in range(rows):
     n=mongo.db.Cantidates.find({})[x]['name']
     g=mongo.db.Cantidates.find({})[x]['gender']
     a=mongo.db.Cantidates.find({})[x]['age']
     v=mongo.db.Cantidates.find({})[x]['vote']
     list2=[n,g,a,v]
     list.append(list2)
    

    return render_template("control.html",data=list,row=rows,col=4);

@app.route('/cpchart',methods=['POST'])

def cpchart():
  reloader =request.values.get("reloader")
  if reloader :
    rows=mongo.db.Cantidates.find().count()
    list =[]
    for x in range(rows):
     n=mongo.db.Cantidates.find({})[x]['name']
     g=mongo.db.Cantidates.find({})[x]['gender']
     a=mongo.db.Cantidates.find({})[x]['age']
     v=mongo.db.Cantidates.find({})[x]['vote']
     list2=[n,g,a,v]
     list.append(list2)
    
    return jsonify({"list":list});
    
@app.route('/register',methods=['POST'])

def register():
    fname =request.form['fname']
    age =request.form['age']
    gender =request.form.get("genders")
    if 'profile_image' in request.files:
      profile_image= request.files['profile_image']  
      if profile_image.filename == "":
       filename="profile.png"
      else:
       unique_filename = str(uuid.uuid4())
       filname, file_extension = os.path.splitext(profile_image.filename)
       mongo.save_file(unique_filename+file_extension,profile_image)
       
       filename=unique_filename+file_extension
       print(filename)
    if fname and gender:
       mongo.db.Cantidates.insert({"name":fname,"gender":gender,"age":age,"vote": int(0),"file":filename})
       print("Inserted")
       #    return jsonify({"name":name});
       # return jsonify({"error":"missing!!!"})
      
       # print(fname,age,gender,unique_filename)
      
       return redirect(url_for("index"));  

@app.route('/delete',methods=['POST'])

def delete():
  delete =request.form['delete']
  if delete :  
    mongo.db.Cantidates.remove({"name":delete})
    print("Deleted")
    return jsonify({"name":delete}); 
  return jsonify({"error":"missing!!!"})
  
@app.route('/voted',methods=['POST'])

def voted():
  vote =request.values.get("vote")
  if vote :
    mongo.db.Cantidates.update({"name":vote},{'$inc':{'vote':1}})
    mongo.db.Users.update({"user":session["user"]},{'$set':{"voted":True}})
    print("Voted")
    return redirect(url_for("login"));    

@app.route('/user')

def user():
 if "user" in session:
   user=session["user"]
   session["online"]= 0
   rows=mongo.db.Cantidates.find().count()
   list =[]
   for x in range(rows):
    n=mongo.db.Cantidates.find({})[x]['name']
    g=mongo.db.Cantidates.find({})[x]['gender']
    a=mongo.db.Cantidates.find({})[x]['age']
    v=mongo.db.Cantidates.find({})[x]['vote']
    i=mongo.db.Cantidates.find({})[x]['file']
    list2=[n,g,a,v,i]
    list.append(list2)
  
   return render_template("index.html",data=list,row=rows,col=5,usr=user)  
 elif session["online"]==0:
    return ("<h1>404 Error</h1>")
 else:
   return ("<h1>404 Error</h1>")


@app.route('/log')
def log():
   return redirect(url_for("user"))

@app.route('/file/<filename>')

def file(filename):
    return mongo.send_file(filename) 